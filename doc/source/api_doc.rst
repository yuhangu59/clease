API Documentation
=========================

.. toctree::
    ./api/settings
    ./api/newstruct
    ./api/evaluate
    ./api/fitting
    ./api/montecarlo
    ./api/data_getters
